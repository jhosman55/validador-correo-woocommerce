<?php
/*
Plugin Name: Validador correo woocommerce
Plugin URI: https://in.jhosman.com
Description: A plugin to add custom fields to Woocommerce checkout and validate them.
Version: 1.0
Author: Jhosman Lizarazo
Author URI: https://in.jhosman.com/
*/

// Evitar acceso directo al archivo
if (!defined('ABSPATH')) {
        exit;
}

// Agregar campo personalizado en la página de checkout
function add_custom_checkout_field($checkout_fields) {
        $checkout_fields['billing']['custom_field'] = array(
                'type' => 'text',
                'label' => __('Validar correo electrónico', 'woocommerce'),
                'placeholder' => __('Ingresa de nuevo el correo', 'woocommerce'),
                'required' => true,
        );
        return $checkout_fields;
}
add_filter('woocommerce_checkout_fields', 'add_custom_checkout_field');

// Validar campo personalizado en la página de checkout
function validate_custom_checkout_field() {
        if ($_POST['custom_field'] !== $_POST['billing_email']) {
                wc_add_notice(__('Los correos ingresados son incorrectos, por favor verificar.', 'woocommerce'), 'error');
        }
}
add_action('woocommerce_checkout_process', 'validate_custom_checkout_field');
